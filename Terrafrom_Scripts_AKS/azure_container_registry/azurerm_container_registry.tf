resource "azurerm_resource_group" "rg_name" {
  name     = var.resource_group_name
  location = var.location
}

resource "azurerm_container_registry" "acr" {
  name                = var.acr_name
  resource_group_name = azurerm_resource_group.rg_name.name
  location            = azurerm_resource_group.rg_name.location
  sku                 = "Premium"
  admin_enabled       = false
  georeplications {
    location                = "West Central US"
    zone_redundancy_enabled = true
    tags                    = {}
  }
  georeplications {
    location                = "West US 2"
    zone_redundancy_enabled = true
    tags                    = {}
  }
}
