from fossology import fossology_token
from fossology.obj import TokenScope
from fossology.obj import AccessLevel
from fossology import Fossology
from fossology.exceptions  import FossologyApiError
import requests

FOSSOLOGY_USER = "fossy"
FOSSOLOGY_PASSWORD = "fossy"
token ="<access_token_fossology>"
FOSSOLOGY_SERVER = "<fossology_server_url>"

foss = Fossology(FOSSOLOGY_SERVER, token, FOSSOLOGY_USER)
print(f"Logged in as user {foss.user.name}")

folder_name = "AwesomeFossFolder"
folder_desc = "AwesomeProjectSources"
test_folder = foss.create_folder(
        foss.rootFolder, folder_name, description=folder_desc
        )
print(f"Created {test_folder.name} with description {test_folder.description}")

group_name = "clearing"
try:
    foss.create_group(group_name)
except FossologyApiError as e:
    if "Details: Group already exists.  Not added." in e.message:
        pass
    else:
        pass
        #raise e
print(f"group named {group_name} is created")

bear = "Bearer "+token


#my_upload1 = foss.upload_file(
#        foss.rootFolder,
#        server=server,
#        description="Upload from SERVER",
#        access_level=AccessLevel.PUBLIC,
#        )

#my_upload1 = foss.upload_file(
#        test_folder,
#        group=group_name,
#        vcs=data,
#        description="Upload from VCS",
#        access_level=AccessLevel.PUBLIC,
#        )
my_upload1 = foss.upload_file(
        test_folder,
        file="mycode.zip",
        description="My product package",
        group=group_name,
        access_level=AccessLevel.PUBLIC,
        )
print(my_upload1)
job_specification = {
        "analysis": {
            "bucket": True,
            "copyright_email_author": True,
            "ecc": True,
            "keyword": True,
            "monk": True,
            "mime": True,
            "monk": True,
            "nomos": True,
            "ojo": True,
            "package": True,
            "specific_agent": True,
            },
        "decider": {
            "nomos_monk": True,
            "bulk_reused": True,
            "new_scanner": True,
            "ojo_decider": True,
            },
        "reuse": {
            "reuse_upload": 0,
            "reuse_group": 0,
            "reuse_main": True,
            "reuse_enhanced": True,
            "reuse_report": True,
            "reuse_copyright": True,
            },
        }
detailed_job = foss.schedule_jobs(
        test_folder,
        my_upload1,
        job_specification
        )
print(f"scan job {detailed_job} set up")

report_id = foss.generate_report(my_upload1, group=group_name)
print(f"report created with id {report_id} ")

content, name = foss.download_report(report_id, group_name)
print(f"report downloaded with name {name}")
with open(name, "wb") as fp:
    len = fp.write(content)
print(f"report was written to file {name}.")
